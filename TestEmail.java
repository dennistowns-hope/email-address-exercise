import static org.junit.Assert.*;

import org.junit.Test;





/** Test case class for testing emails address verification.

   This source code is from the book 
     "Flexible, Reliable Software:
       Using Patterns and Agile Development"
     published 2010 by CRC Press.
   Author: 
     Henrik B Christensen 
     Computer Science Department
     Aarhus University
   
   This source code is provided WITHOUT ANY WARRANTY either 
   expressed or implied. You may study, use, modify, and 
   distribute it for non-commercial purposes. For any 
   commercial use, see http://www.baerbak.com/
*/
public class TestEmail {

  private EmailAddress address;

  @Test
  public void replaceMe() {
    assert(true);
  }

  @Test
  public void shouldBeAcceptable() {
    String[] strings = new String[4];
    strings[0] = "john@cs.edu";
    strings[1] ="IC3YCOLD@dad.s6.frame.com";
    strings[2] = "TB12@flame.r32.edu";
    strings[3] = "big@small.gov";
    for(String str : strings) {
    	EmailAddress ea = new EmailAddress(str);
    	assertTrue(ea.isValid() == true);
    }
   
  }
  @Test
  public void shouldBeUnacceptable() {
	  String[] strings = new String[4];
	    strings[0] = "1ohn@cs.edu";
	    strings[1] ="IC3YCOLD@1dad.3s6.frame.com";
	    strings[2] = "TB12flame.r32.edu";
	    strings[3] = "big@smallgov.";
	    for(String str : strings) {
	    	EmailAddress ea = new EmailAddress(str);
	    	System.out.println(ea.address);
	    	assertFalse(ea.isValid() == true);
	    }
  }
}
